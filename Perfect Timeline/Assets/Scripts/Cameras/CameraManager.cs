using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    public GameObject[] Cameras;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        Cameras = GameObject.FindGameObjectsWithTag("VirtualCamera");
        ResetCameras();
    }

    private void Start()
    {
        Cameras = GameObject.FindGameObjectsWithTag("VirtualCamera");
        ResetCameras();
    }


    public void ResetCameras()
    {
        foreach (GameObject cam in Cameras)
        {
            cam.GetComponent<CinemachineVirtualCamera>().Priority = 0;
        }
    }
}
