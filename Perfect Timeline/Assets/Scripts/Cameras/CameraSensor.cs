using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSensor : MonoBehaviour
{
    [SerializeField] GameObject Camera;
    CinemachineVirtualCamera VC;

    private void Start()
    {
        VC = Camera.GetComponent<CinemachineVirtualCamera>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            CameraManager.Instance.ResetCameras();
            VC.Priority = 10;
        }
    }
}
