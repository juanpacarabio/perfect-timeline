using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{
    public Transform player;             
    public GameObject projectilePrefab;  
    public Transform firePoint;          
    public float detectionRange = 10f;   
    public float fireCooldown = 2f;      
    public float projectileForce = 10f;  

    private float lastFireTime;

    private void Start()
    {
        player = PlayerController.Instance.gameObject.transform;
    }

    void Update()
    {
        RotateTowardsPlayer();
        CheckForPlayerAndFire();
    }

    void RotateTowardsPlayer()
    {
        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    void CheckForPlayerAndFire()
    {
        if (Time.time > lastFireTime + fireCooldown)
        {
            Vector2 direction = player.position - firePoint.position;
            RaycastHit2D hit = Physics2D.Raycast(firePoint.position, direction, detectionRange);

            Debug.Log(hit.collider.gameObject);

            if (hit.collider != null && hit.collider.CompareTag("Player"))
            {
                FireProjectile(direction);
                lastFireTime = Time.time;
            }
        }
    }

    void FireProjectile(Vector2 direction)
    {
        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = direction.normalized * projectileForce;
        }
    }
}
