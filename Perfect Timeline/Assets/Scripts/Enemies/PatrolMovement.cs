using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolMovement : MonoBehaviour
{
    public GameObject targetObject;

    void Update()
    {
        if (targetObject != null)
        {
            transform.position = targetObject.transform.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.Instance.TakeDamage(this.transform, targetObject.GetComponent<EnemyScript>().DamageNumber);
        }
    }
}
