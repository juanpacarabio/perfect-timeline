using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingDamageNumber : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject,0.6f);
        transform.localPosition += new Vector3(0, 0.5f, 0);
    }
}
