using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stomp : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("WeakPoint"))
        {
            Destroy(collision.gameObject.transform.parent.gameObject);
            PlayerController.Instance.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(PlayerController.Instance.gameObject.GetComponent<Rigidbody2D>().velocity.x, 10);
        }
    }
}
