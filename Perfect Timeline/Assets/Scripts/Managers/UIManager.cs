using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Header("PowerUps")]
    [SerializeField] GameObject DoubleJump;
    [SerializeField] GameObject Dash;
    [SerializeField] GameObject WallJump;
    [SerializeField] GameObject Rewind;

    bool UsedPowers = false;

    [Header("FinishLevel")]
    [SerializeField] TMPro.TextMeshProUGUI TimeSpent, ParadoxLevel;
    [SerializeField] GameObject EndLevelScreen, SuccessScreen, GameOverScreen, UsedPowerups, UsedDJump, UsedDash, UsedWJump, UsedRewind;
    public int Paradox;
    public float Time;

    public static UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        ClearUI();
        CheckAndDisplay();
    }

    public void CheckAndDisplay()
    {
        if (GameManager.Instance.HasDoubleJump == true)
        {
            DoubleJump.SetActive(true);
            UsedPowers = true;
        }else if (GameManager.Instance.HasDoubleJump == false)
        {
            DoubleJump.SetActive(false);
        }
        if (GameManager.Instance.HasDash == true)
        {
            Dash.SetActive(true);
            UsedPowers=true;
        }
        else if (GameManager.Instance.HasDash == false)
        {
            Dash.SetActive(false);
        }
        if (GameManager.Instance.HasWallJump == true)
        {
            WallJump.SetActive(true);
            UsedPowers = true;
        }
        else if (GameManager.Instance.HasWallJump == false)
        {
            WallJump.SetActive(false);
        }
        if (GameManager.Instance.HasRewind == true)
        {
            Rewind.SetActive(true);
            UsedPowers=true;
        }else if (GameManager.Instance.HasRewind== false)
        {
            Rewind.SetActive(false);
        }
    }

    public void ClearUI()
    {      
        UsedDJump.SetActive(false);
        UsedDash.SetActive(false);
        UsedWJump.SetActive(false);
        UsedPowerups.SetActive(false);
        SuccessScreen.SetActive(false);
        GameOverScreen.SetActive(false);
        EndLevelScreen.SetActive(false);
    }

    public void FinishLevel()
    {
        EndLevelScreen.SetActive(true);
        SuccessScreen.SetActive(true);

        if (UsedPowers == true)
        {
            UsedPowerups.SetActive(true);
            if (DoubleJump.gameObject.activeSelf == true)
            {
                UsedDJump.SetActive(true);
            }

            if (Dash.gameObject.activeSelf == true)
            {
                UsedDash.SetActive(true);
            }

            if (WallJump.gameObject.activeSelf == true)
            {
                UsedWJump.SetActive(true);
            }

            if (Rewind.gameObject.activeSelf == true)
            {
                UsedRewind.SetActive(true);
            }
        }else if (UsedPowers == false)
        {
            UsedPowerups.SetActive(false);
        }

        ParadoxLevel.text = Paradox.ToString();

        float minutes = Mathf.FloorToInt(Time / 60);
        float seconds = Mathf.FloorToInt(Time % 60);

        TimeSpent.text = string.Format("{0:00}:{1:00}", minutes, seconds);

    }

    public void LoseLevel()
    {
        EndLevelScreen.SetActive(true);
        GameOverScreen.SetActive(true);
    }

    public void ReturnToRooT()
    {
        Cursor.visible = false;
        GameManager.Instance.GameIsPaused = false;
        SceneManager.LoadScene("PlaceholderROOT");
    }

    public void ReturnToMenu()
    {
        Cursor.visible = true;
        GameManager.Instance.GameIsPaused = false;
        SceneManager.LoadScene("MenuScene");
    }

}
