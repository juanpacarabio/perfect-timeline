using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralTimerScript : MonoBehaviour
{
    float TimeShown;
    [SerializeField] TMPro.TextMeshPro Timer;
    void Update()
    {
        GameManager.Instance.CalculateTimeLeft();
        TimeShown = GameManager.Instance.RemainingTime;
        float minutes = Mathf.FloorToInt(TimeShown / 60);
        float seconds = Mathf.FloorToInt(TimeShown % 60);
        Timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
