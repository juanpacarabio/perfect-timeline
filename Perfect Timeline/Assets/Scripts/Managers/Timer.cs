using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float RemainingTime;
    public float TimeSpent;
    public bool TimerActive;
    public TMPro.TextMeshProUGUI TimerText;
    public static Timer Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        if (LevelManager.Instance != null)
        {
            if (LevelManager.Instance.LevelCompleted == true)
            {
                if (LevelManager.Instance.LevelCode == 0)
                {
                    RemainingTime = GameManager.Instance.TotalTime;
                }
                else
                {
                    RemainingTime = GameManager.Instance.TimeAfterLevels[LevelManager.Instance.LevelCode - 1];
                }

            }
            else
            {
                if (LevelManager.Instance.LevelCode == 0)
                {
                    RemainingTime = GameManager.Instance.TotalTime;
                }
                else
                {
                    RemainingTime = GameManager.Instance.TimeAfterLevels[LevelManager.Instance.LevelCode - 1];
                }


            }
        }
        
    }
    void Update()
    {
        
        if (TimerActive== true)
        {
            if (RemainingTime > 0)
            {
                RemainingTime -= Time.deltaTime;
                TimeSpent += Time.deltaTime;
            }
            else
            {
                RemainingTime = 0;
                LevelManager.Instance.Lose();
            }
        }

        ShowTime(RemainingTime);
    }

    public void ShowTime(float TimeToShow)
    {
        if (TimeToShow < 0)
        {
            TimeToShow = 0;
        }

        float minutes = Mathf.FloorToInt(TimeToShow / 60);
        float seconds = Mathf.FloorToInt(TimeToShow % 60);

        TimerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
