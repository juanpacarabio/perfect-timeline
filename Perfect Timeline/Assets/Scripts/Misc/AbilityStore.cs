using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityStore : MonoBehaviour
{
    [SerializeField] bool SellsDoubleJump;
    [SerializeField] bool SellsDash;
    [SerializeField] bool SellsWallJump;
    [SerializeField] bool SellsRewind;
    [SerializeField] TMPro.TextMeshPro ItemName, ItemNameBig;
    [SerializeField] TMPro.TextMeshPro ItemCost;
    [SerializeField] GameObject DJumpSmall, DashSmall, WJumpSmall, RewindSmall, DJumpBig, DashBig, WJumpBig, RewindBig, InfoScreen, HasPowerSmall, HasntPowerSmall, HasPowerBig, HasntPowerBig;
    bool PlayerOwnsPower = false;
    private void Start()
    {
        Clear();
        if (SellsDoubleJump)
        {
            ItemName.text = "Double Jump";
            DJumpSmall.SetActive(true);
        }
        if (SellsDash)
        {
            ItemName.text = "Dash";
            DashSmall.SetActive(true);
        }
        if (SellsWallJump)
        {
            ItemName.text = "Wall Jump";
            WJumpSmall.SetActive(true);
        }
        if (SellsRewind)
        {
            ItemName.text = "Rewind";
            RewindSmall.SetActive(true);
        }
        BuyOrSell();
    }

    private void Update()
    {
        if (SellsDoubleJump)
        {
            PlayerOwnsPower = PlayerController.Instance.hasDoubleJump;
        }
        if (SellsDash)
        {
            PlayerOwnsPower = PlayerController.Instance.hasDash;
        }
        if (SellsWallJump)
        {
            PlayerOwnsPower = PlayerController.Instance.hasWallJump;
        }
        if (SellsRewind)
        {
            PlayerOwnsPower = PlayerController.Instance.HasRewind;
        }

        if (InfoScreen.activeSelf == true)
        {
            if (PlayerOwnsPower)
            {
                HasPowerBig.SetActive(true);
                HasntPowerBig.SetActive(false);
            }else if (PlayerOwnsPower == false)
            {
                HasPowerBig.SetActive(false);
                HasntPowerBig.SetActive(true);
            }
        }
    }

    void Clear()
    {
        DJumpSmall.SetActive(false);
        DashSmall.SetActive(false);
        WJumpSmall.SetActive(false);
        RewindSmall.SetActive(false);
        DJumpBig.SetActive(false);
        DashBig.SetActive(false);
        WJumpBig.SetActive(false);
        RewindBig.SetActive(false);
        HasPowerSmall.SetActive(false);
        HasntPowerSmall.SetActive(false);
        HasPowerBig.SetActive(false);
        HasntPowerBig.SetActive(false);
        InfoScreen.SetActive(false);
        ItemNameBig.gameObject.SetActive(false);
    }

    public void BuyOrSell()
    {
        
        if (SellsDoubleJump == true)
        {
            ItemCost.text = GameManager.Instance.DoubleJumpCost.ToString() + "s";
        }

        if (SellsDash == true)
        {
            ItemCost.text = GameManager.Instance.DashCost.ToString() + "s";
        }
        
        if (SellsWallJump == true)
        {
            ItemCost.text = GameManager.Instance.WallJumpCost.ToString() + "s";
        }

        if (SellsRewind == true)
        {
            ItemCost.text = GameManager.Instance.RewindCost.ToString() + "s";
        }

        ItemCost.color = Color.red;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.Instance.PowerSelected = this.gameObject;
        }

        DJumpSmall.SetActive(false);
        DashSmall.SetActive(false);
        WJumpSmall.SetActive(false);
        RewindSmall.SetActive(false);
        HasPowerSmall.SetActive(false);
        HasntPowerSmall.SetActive(false);
        ItemNameBig.text = ItemName.text;
        InfoScreen.SetActive(true);
        ItemNameBig.gameObject.SetActive(true);
        if (PlayerOwnsPower == true)
        {
            HasPowerBig.SetActive(true);
            HasntPowerBig.SetActive(false);
        }else if (PlayerOwnsPower == false)
        {
            HasPowerBig.SetActive(false);
            HasntPowerBig.SetActive(true);
        }

        if (SellsDoubleJump == true)
        {
            DJumpBig.SetActive(true);
        }else if (SellsDoubleJump == false)
        {
            DJumpBig.SetActive(false);
        }

        if (SellsDash == true)
        {
            DashBig.SetActive(true);
        }else if (SellsDash == false)
        {
            DashBig.SetActive(false);
        }

        if (SellsWallJump == true)
        {
            WJumpBig.SetActive(true);
        }else if (SellsWallJump == false)
        {
            WJumpBig.SetActive(false);
        }

        if (SellsRewind == true)
        {
            RewindBig.SetActive(true);
        }else if (SellsRewind == false)
        {
            RewindBig.SetActive(false);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.Instance.PowerSelected = null;
            InfoScreen.SetActive(false);

            if (PlayerOwnsPower == true)
            {
                HasPowerSmall.SetActive(true);
                HasntPowerSmall.SetActive(false);
            }
            else if (PlayerOwnsPower == false)
            {
                HasPowerSmall.SetActive(false);
                HasntPowerSmall.SetActive(true);
            }

            if (SellsDoubleJump == true)
            {
                DJumpSmall.SetActive(true);
            }
            else if (SellsDoubleJump == false)
            {
                DJumpSmall.SetActive(false);
            }

            if (SellsDash == true)
            {
                DashSmall.SetActive(true);
            }
            else if (SellsDash == false)
            {
                DashSmall.SetActive(false);
            }

            if (SellsWallJump == true)
            {
                WJumpSmall.SetActive(true);
            }
            else if (SellsWallJump == false)
            {
                WJumpSmall.SetActive(false);
            }

            if (SellsRewind == true)
            {
                RewindSmall.SetActive(true);
            }else if(SellsRewind == false)
            {
                RewindSmall.SetActive(false);
            }

        }
    }

    public void AddOrRemovePower()
    {
        if (SellsDoubleJump)
        {
            if (GameManager.Instance.HasDoubleJump == true)
            {
                GameManager.Instance.HasDoubleJump = false;
                PlayerController.Instance.hasDoubleJump = false;
            }
            else if (GameManager.Instance.HasDoubleJump == false)
            {
                if (GameManager.Instance.RemainingTime > GameManager.Instance.DoubleJumpCost)
                {
                    GameManager.Instance.HasDoubleJump = true;
                    PlayerController.Instance.hasDoubleJump = true;
                }
                else
                {

                }
            }
        }

        if (SellsDash)
        {
            if (GameManager.Instance.HasDash == true)
            {
                GameManager.Instance.HasDash = false;
                PlayerController.Instance.hasDash = false;
            }
            else if (GameManager.Instance.HasDash == false)
            {
                if (GameManager.Instance.RemainingTime > GameManager.Instance.DashCost)
                {
                    GameManager.Instance.HasDash = true;
                    PlayerController.Instance.canDash = true;
                    PlayerController.Instance.hasDash = true;
                }
                else
                {

                }
            }
        }

        if (SellsWallJump)
        {
            if (GameManager.Instance.HasWallJump == true)
            {
                GameManager.Instance.HasWallJump = false;
                PlayerController.Instance.hasWallJump = false;
            }
            else if (GameManager.Instance.HasWallJump == false)
            {
                if (GameManager.Instance.RemainingTime > GameManager.Instance.WallJumpCost)
                {
                    GameManager.Instance.HasWallJump = true;
                    PlayerController.Instance.hasWallJump = true;
                }
                else
                {

                }
            }
        }

        if (SellsRewind)
        {
            if (GameManager.Instance.HasRewind == true)
            {
                GameManager.Instance.HasRewind = false;
                PlayerController.Instance.HasRewind = false;
            }
            else if (GameManager.Instance.HasRewind == false)
            {
                if (GameManager.Instance.RemainingTime > GameManager.Instance.RewindCost)
                {
                    GameManager.Instance.HasRewind = true;
                    PlayerController.Instance.HasRewind = true;
                    PlayerController.Instance.StartSaving();
                }
                else
                {

                }
            }
        }

        BuyOrSell();
        UIManager.Instance.CheckAndDisplay();
    }
}
