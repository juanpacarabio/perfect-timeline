using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyMovement : BasicEntityMovement
{
    Vector3 PreviousPosition;
    Vector3 CurrentPosition;
    public override void Update()
    {
        base.Update();

        CurrentPosition = transform.position;

        if (PreviousPosition == null)
        {
            PreviousPosition = CurrentPosition;
        }

        if (PreviousPosition.x < CurrentPosition.x)
        {
            transform.localScale = new Vector3(1,1,1);
        }else if (PreviousPosition.x > CurrentPosition.x)
        {
            transform.localScale = new Vector3(-1,1,1);
        }else if (PreviousPosition.x == CurrentPosition.x)
        {

        }

        PreviousPosition = CurrentPosition;

    }
}
