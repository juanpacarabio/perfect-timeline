using System.Collections;
using System.Collections.Generic;
//using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelDoors : MonoBehaviour
{
    [Tooltip("Code representing the level this door leads to. Starting from 0 for the first level onwards.")]
    [SerializeField] private int LevelCode;
    [Tooltip("The name of the level Scene.")]
    [SerializeField] private string LevelName;
    [Tooltip("The best time the player has accomplished for this level.")]
    public float TimeSpent;
    [Tooltip("Total time remaining after complition of this level.")]
    public float TimeAfterLevel;
    public bool LevelCompleted;
    public TMPro.TextMeshPro TimeSpentText;
    public TMPro.TextMeshPro TimeAfterLevelText;

    [SerializeField] GameObject DoubleJump;
    [SerializeField] GameObject Dash;
    [SerializeField] GameObject WallJump;
    [SerializeField] GameObject Rewind;


    [SerializeField] GameObject InfoDisplay;
    [SerializeField] TMPro.TextMeshPro DisplayTimeSpent, DisplayParadoxLevel;
    [SerializeField] GameObject DisplayTimeOnTrigger, DisplayParadox, JumpMessageUnplayed, JumpMessage, DisplayDash, DisplayDoubleJump, DisplayWallJump, DisplayRewind;
    private void Start()
    {
        DisableInfoDisplay();
        if (GameManager.Instance.TimeSpent.Count > LevelCode)
        {
            LevelCompleted = true;
            GameManager.Instance.RecalculateTimes();
        }
        else 
        {
            LevelCompleted = false;
        }

        if (LevelCompleted == true)
        {
            if (GameManager.Instance.UsedDoubleJump[LevelCode] == true)
            {
                DoubleJump.SetActive(true);
            }else if(GameManager.Instance.UsedDoubleJump[LevelCode] == false)
            {
                DoubleJump.SetActive(false);
            }

            if (GameManager.Instance.UsedDash[LevelCode] == true)
            {
                Dash.SetActive(true);
            }
            else if (GameManager.Instance.UsedDash[LevelCode] == false)
            {
                Dash.SetActive(false);
            }
            if (GameManager.Instance.UsedWallJump[LevelCode] == true)
            {
                WallJump.SetActive(true);
            }
            else if (GameManager.Instance.UsedWallJump[LevelCode] == false)
            {
                WallJump.SetActive(false);
            }
            if (GameManager.Instance.UsedRewind[LevelCode] == true)
            {
                Rewind.SetActive(true);
            }else if (GameManager.Instance.UsedRewind[LevelCode] == false)
            {
                Rewind.SetActive(false);
            }
        }
        

    }
    private void Update()
    {
        if (LevelCompleted == true)
        {
            if (LevelCode == 0)
            {
                TimeSpentText.enabled = true;
                TimeAfterLevelText.enabled = true;
                TimeSpent = GameManager.Instance.TimeSpent[LevelCode];
                TimeAfterLevel = GameManager.Instance.TotalTime - TimeSpent;
                DisplayTime();
                 if (GameManager.Instance.UsedDoubleJump[LevelCode] == true)
            {
                DoubleJump.SetActive(true);
            }else if(GameManager.Instance.UsedDoubleJump[LevelCode] == false)
            {
                DoubleJump.SetActive(false);
            }

            if (GameManager.Instance.UsedDash[LevelCode] == true)
            {
                Dash.SetActive(true);
            }
            else if (GameManager.Instance.UsedDash[LevelCode] == false)
            {
                Dash.SetActive(false);
            }
            if (GameManager.Instance.UsedWallJump[LevelCode] == true)
            {
                WallJump.SetActive(true);
            }
            else if (GameManager.Instance.UsedWallJump[LevelCode] == false)
            {
                WallJump.SetActive(false);
            }
            }
            else
            {
                TimeSpentText.enabled = true;
                TimeAfterLevelText.enabled = true;
                TimeSpent = GameManager.Instance.TimeSpent[LevelCode];
                TimeAfterLevel = GameManager.Instance.TimeAfterLevels[LevelCode-1] - TimeSpent;
                DisplayTime();
            }

            if (GameManager.Instance.UsedDoubleJump[LevelCode] == true)
            {
                DoubleJump.SetActive(true);
            }
            else if (GameManager.Instance.UsedDoubleJump[LevelCode] == false)
            {
                DoubleJump.SetActive(false);
            }

            if (GameManager.Instance.UsedDash[LevelCode] == true)
            {
                Dash.SetActive(true);
            }
            else if (GameManager.Instance.UsedDash[LevelCode] == false)
            {
                Dash.SetActive(false);
            }
            if (GameManager.Instance.UsedWallJump[LevelCode] == true)
            {
                WallJump.SetActive(true);
            }
            else if (GameManager.Instance.UsedWallJump[LevelCode] == false)
            {
                WallJump.SetActive(false);
            }
        }
        else if(LevelCompleted == false)
        {
            TimeSpentText.enabled = false;
            TimeAfterLevelText.enabled = false;
        }
    }

    public void DisplayTime()
    {
        if (LevelCompleted == false)
        {
            TimeAfterLevel = GameManager.Instance.RemainingTime;
            TimeSpent = 0;
        }
        else
        {
            float SpentMinutes = Mathf.FloorToInt(TimeSpent / 60);
            float SpentSeconds = Mathf.FloorToInt(TimeSpent % 60);
            TimeSpentText.text = string.Format("{0:00}:{1:00}", SpentMinutes, SpentSeconds);
        }

        float minutes = Mathf.FloorToInt(TimeAfterLevel / 60);
        float seconds = Mathf.FloorToInt(TimeAfterLevel % 60);
        TimeAfterLevelText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            if (LevelCode == 0)
            {
                PlayerController.Instance.LevelSelected = this.gameObject;
                if (LevelCompleted == false)
                {
                    InfoDisplay.SetActive(true);
                    JumpMessageUnplayed.SetActive(true);
                }
                else if (LevelCompleted == true)
                {
                    InfoDisplay.SetActive(true);
                    DisplayTimeOnTrigger.SetActive(true);
                    JumpMessage.SetActive(true);

                    float minutes = Mathf.FloorToInt(TimeSpent / 60);
                    float seconds = Mathf.FloorToInt(TimeSpent % 60);

                    DisplayTimeSpent.text = string.Format("{0:00}:{1:00}", minutes, seconds);

                    if (GameManager.Instance.TimesBeaten[LevelCode] > 0)
                    {
                        DisplayParadox.SetActive(true);
                        DisplayParadoxLevel.text = GameManager.Instance.TimesBeaten[LevelCode].ToString();
                    }
                    if (GameManager.Instance.UsedDoubleJump[LevelCode] == true)
                    {
                        DisplayDoubleJump.SetActive(true);
                    }
                    if (GameManager.Instance.UsedDash[LevelCode] == true)
                    {
                        DisplayDash.SetActive(true);
                    }
                    if (GameManager.Instance.UsedWallJump[LevelCode] == true)
                    {
                        DisplayWallJump.SetActive(true);
                    }
                    if (GameManager.Instance.UsedRewind[LevelCode] == true)
                    {
                        DisplayRewind.SetActive(true);
                    }
                }
            }else if (GameManager.Instance.TimeSpent[LevelCode - 1] != 0)
            {
                PlayerController.Instance.LevelSelected = this.gameObject;
                if (LevelCompleted == false)
                {
                    InfoDisplay.SetActive(true);
                    JumpMessageUnplayed.SetActive(true);
                }
                else if (LevelCompleted == true)
                {
                    InfoDisplay.SetActive(true);
                    DisplayTimeOnTrigger.SetActive(true);
                    JumpMessage.SetActive(true);

                    float minutes = Mathf.FloorToInt(TimeSpent / 60);
                    float seconds = Mathf.FloorToInt(TimeSpent % 60);

                    DisplayTimeSpent.text = string.Format("{0:00}:{1:00}", minutes, seconds);

                    if (GameManager.Instance.TimesBeaten[LevelCode] > 0)
                    {
                        DisplayParadox.SetActive(true);
                        DisplayParadoxLevel.text = GameManager.Instance.TimesBeaten[LevelCode].ToString();
                    }
                    if (GameManager.Instance.UsedDoubleJump[LevelCode] == true)
                    {
                        DisplayDoubleJump.SetActive(true);
                    }
                    if (GameManager.Instance.UsedDash[LevelCode] == true)
                    {
                        DisplayDash.SetActive(true);
                    }
                    if (GameManager.Instance.UsedWallJump[LevelCode] == true)
                    {
                        DisplayWallJump.SetActive(true);
                    }
                    if (GameManager.Instance.UsedRewind[LevelCode] == true)
                    {
                        DisplayRewind.SetActive(true);
                    }
                }
            }   
        }
    }



    private void OnTriggerExit2D(Collider2D collision)
    {
        DisableInfoDisplay();
        PlayerController.Instance.LevelSelected = null;
    }

    public void DisableInfoDisplay()
    {
        DisplayTimeOnTrigger.SetActive(false);
        DisplayParadox.SetActive(false);
        JumpMessage.SetActive(false);
        JumpMessageUnplayed.SetActive(false);
        DisplayDash.SetActive(false);
        DisplayDoubleJump.SetActive(false);
        DisplayWallJump.SetActive(false);
        DisplayRewind.SetActive(false);
        InfoDisplay.SetActive(false);
    }

    public void StartLevel()
    {
        if (LevelCode == 0)
        {
            SceneManager.LoadScene(LevelName);
        }
        else if (LevelCode != 0 && GameManager.Instance.TimeSpent[LevelCode - 1] != 0)
        {
            SceneManager.LoadScene(LevelName);
        }
    }
}
