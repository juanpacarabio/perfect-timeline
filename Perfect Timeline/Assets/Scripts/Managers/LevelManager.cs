using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public int LevelCode;
    public bool LevelCompleted;
    float TimeSpent;
    float TimeBeforeLevel;
    float TimeAfterLevel;

    #region Paradox System
    int ParadoxLevel;
    List<GameObject> Anomalies;
    public bool SpecialCase = false;
    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Cursor.visible = false;
        if (GameManager.Instance.TimeSpent.Count > LevelCode)
        {
            LevelCompleted = true;
        }
        else
        {
            LevelCompleted = false;
        }

        if (LevelCompleted == true)
        {
            if (LevelCode == 0)
            {
                TimeBeforeLevel = GameManager.Instance.TotalTime;
            }
            else
            {
                TimeBeforeLevel = GameManager.Instance.TimeAfterLevels[LevelManager.Instance.LevelCode - 1];
            }
            ParadoxLevel = GameManager.Instance.TimesBeaten[LevelCode];
        }
        else
        {
            TimeBeforeLevel = GameManager.Instance.RemainingTime;
        }     
        ActivateAnomalies();

    }

    private void Update()
    {
        TimeSpent = Timer.Instance.TimeSpent;
    }
    public void FinishLevel()
    {
        Cursor.visible = true;
        GameManager.Instance.GameIsPaused = true;
        if (GameManager.Instance.HasDoubleJump == true)
        {
            TimeSpent = TimeSpent + GameManager.Instance.DoubleJumpCost;
        }
        if (GameManager.Instance.HasDash == true)
        {
            TimeSpent = TimeSpent + GameManager.Instance.DashCost;
        }
        if (GameManager.Instance.HasWallJump == true)
        {
            TimeSpent = TimeSpent + GameManager.Instance.WallJumpCost;
        }
        if (GameManager.Instance.HasRewind == true)
        {
            TimeSpent = TimeSpent + GameManager.Instance.RewindCost;
        }

        CalculateTimeAfterLevel();

        if (LevelCompleted == false)
        {
            GameManager.Instance.TimeSpent.Add(TimeSpent);
            GameManager.Instance.TimeAfterLevels.Add(TimeAfterLevel);
            GameManager.Instance.TimesBeaten.Add(0);

            if (GameManager.Instance.HasDoubleJump == true)
            {
                GameManager.Instance.UsedDoubleJump.Add(true);
            }else if (GameManager.Instance.HasDoubleJump == false)
            {
                GameManager.Instance.UsedDoubleJump.Add(false);
            }
            if (GameManager.Instance.HasDash == true)
            {
                GameManager.Instance.UsedDash.Add(true);
            }
            else if (GameManager.Instance.HasDash == false)
            {
                GameManager.Instance.UsedDash.Add(false);
            }
            if (GameManager.Instance.HasWallJump == true)
            {
                GameManager.Instance.UsedWallJump.Add(true);
            }
            else if (GameManager.Instance.HasWallJump == false)
            {
                GameManager.Instance.UsedWallJump.Add(false);
            }
            if (GameManager.Instance.HasRewind == true)
            {
                GameManager.Instance.UsedRewind.Add(true);
            }else if (GameManager.Instance.HasRewind == false)
            {
                GameManager.Instance.UsedRewind.Add(false);
            }
        }
        else if (LevelCompleted == true)
        {
            if (TimeSpent < GameManager.Instance.TimeSpent[LevelCode])
            {
                GameManager.Instance.TimeSpent[LevelCode] = TimeSpent;
                GameManager.Instance.TimeAfterLevels[LevelCode] = TimeAfterLevel;
                GameManager.Instance.TimesBeaten[LevelCode]++;
                UIManager.Instance.Paradox = GameManager.Instance.TimesBeaten[LevelCode];
                if (GameManager.Instance.HasDoubleJump == true)
                {
                    GameManager.Instance.UsedDoubleJump[LevelCode] = true;
                }
                else if (GameManager.Instance.HasDoubleJump == false)
                {
                    GameManager.Instance.UsedDoubleJump[LevelCode] = false;
                }
                if (GameManager.Instance.HasDash == true)
                {
                    GameManager.Instance.UsedDash[LevelCode] = true;
                }
                else if (GameManager.Instance.HasDash == false)
                {
                    GameManager.Instance.UsedDash[LevelCode] = false;
                }
                if (GameManager.Instance.HasWallJump == true)
                {
                    GameManager.Instance.UsedWallJump[LevelCode] = true;
                }
                else if (GameManager.Instance.HasWallJump == false)
                {
                    GameManager.Instance.UsedWallJump[LevelCode] = false;
                }
                if (GameManager.Instance.HasRewind == true)
                {
                    GameManager.Instance.UsedRewind[LevelCode] = true;
                }else if (GameManager.Instance.HasRewind== false)
                {
                    GameManager.Instance.UsedRewind[LevelCode]= false;
                }

            }

        }

        Timer.Instance.TimerActive = false;
        UIManager.Instance.Time = TimeSpent;
        UIManager.Instance.FinishLevel();
    }

    public void Lose()
    {
        Cursor.visible = true;
        GameManager.Instance.GameIsPaused = true;
        Timer.Instance.TimerActive = false;
        UIManager.Instance.LoseLevel();
    }

    public void CalculateTimeAfterLevel()
    {
        float Time = TimeBeforeLevel;

        if (LevelCode == 0)
        {
            Time = GameManager.Instance.TotalTime - TimeSpent;
        }
        else
        {
            Time = GameManager.Instance.TimeAfterLevels[LevelCode - 1] - TimeSpent;
        }
        TimeAfterLevel = Time;

    }

    public void ActivateAnomalies()
    {
        if (SpecialCase == false)
        {
            GameObject[] Anomalies = GameObject.FindGameObjectsWithTag("Anomaly");
            for (int i = 0; i < Anomalies.Length; i++)
            {
                Anomalies[i].SetActive(false);
            }

            if (ParadoxLevel > 0)
            {
                for (int i = 0; i < ParadoxLevel; i++)
                {
                    if (i <= Anomalies.Length)
                    {
                        int a = SelectAnomaly(Anomalies);
                        Anomalies[a].SetActive(true);
                    }
                }
            }
            
        }else if (SpecialCase == true)
        {

        }

    }

    public int SelectAnomaly(GameObject[] Anomalies)
    {
        int a = Random.Range(0, Anomalies.Length);
        if (Anomalies[a].activeSelf == false)
        {
            return a;
        }
        else
        {
            a = SelectAnomaly(Anomalies);
            return a;
        }
        
    }


}
