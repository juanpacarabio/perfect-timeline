using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    #region Player Stats
    [Header("General Stats")]
    [SerializeField] private float speed;
    [Header("Jump Stats")]
    [SerializeField] private float JumpStrength;
    [SerializeField] private float CoyoteTime;
    private bool IsJumping = false;
    private bool canJump;
    bool grounded;
    [SerializeField] private float JumpBufferTime;
    private float JumpBufferCounter;
    #endregion
    #region Abilities
    [Header("Double Jump")]
    public bool hasDoubleJump = false;
    public bool SecondJump;
    public int UsedSecondJump;
    [Header("Dash")]
    public bool hasDash = false;
    public bool canDash;
    private bool isDashing = false;
    public float DashStrength;
    private float DashLength = 0.2f;
    public float DashCooldown;
    [Header("Blade")]
    public bool hasBlade = false;
    [Header("Hook")]
    public bool hasHook = false;
    [Header("Wall Jump/Slide")]
    public bool hasWallJump = false;
    private bool Sliding;
    public float SlidingSpeed;
    private bool WallJumping;
    private float WallJumpDirection;
    private float WallJumpTime = 0.2f;
    private float WallJumpCounter;
    private float WallJumpDuration = 0.4f;
    public Vector2 WallJumpStrength;
    [Header("Rewind")]
    public bool HasRewind = false;
    public float rewindCooldown = 4f;
    public float rewindInterval = 1f;
    public float rewindSpeed = 2f;
    public float rewindTime = 3f;

    private List<Vector3> rewindPositions = new List<Vector3>();
    private bool isRewinding = false;
    private bool RewindOnCooldown = false;

    [Header("")]

    #endregion
    #region Enemy Interactions
    public float KnockBackSTR;
    public float KnockBackTimer;
    public float KnockBackTime;
    public bool HitFromRight;
    #endregion
    #region Components
    private Rigidbody2D body;
    BoxCollider2D Collider;
    Vector2 move;
    [SerializeField] private Transform WallSensor;
    [SerializeField] TrailRenderer Trail;
    #endregion
    #region Technical
    private float horizontalMov;
    private bool facingRight = true;
    public static PlayerController Instance;
    public GameObject LevelSelected = null;
    [SerializeField] GameObject DamageTaken;
    public GameObject PowerSelected = null;
    #endregion
    #region Layers
    [SerializeField] private LayerMask GroundLayer;
    [SerializeField] private LayerMask WallLayer;
    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        body = GetComponent<Rigidbody2D>();
        Collider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        hasDoubleJump = false;
        hasDash = false;
        hasWallJump = false;

        if (GameManager.Instance.HasDoubleJump == true)
        {
            hasDoubleJump = true;
        }
        if (GameManager.Instance.HasDash == true)
        {
            hasDash = true;
        }
        if (GameManager.Instance.HasWallJump == true)
        {
            hasWallJump = true;
        }
        if (GameManager.Instance.HasRewind == true)
        {
            HasRewind = true;
            StartCoroutine(SavePosition());
        }
        if (hasDash)
        {
            canDash = true;
        }
    }
    public bool testGround;
    private void Update()
    {
        if (GameManager.Instance.GameIsPaused == false)
        {
            if (OnGround())
            {
                canJump = true;
                IsJumping = false;
                SecondJump = false;
                UsedSecondJump = 0;
            }

            if (IsJumping == false)
            {
                if (grounded != OnGround())
                {                   
                    StartCoroutine(CoyoteTimeCountdown());
                }
            }
            else
            {
                canJump = false;
            }

            grounded = OnGround();

            if (KnockBackTimer <= 0)
            {
                if (WallJumping == false && isDashing == false && isRewinding == false)
                {
                    if (!facingRight && horizontalMov > 0f)
                    {
                        Flip();
                    }
                    else if (facingRight && horizontalMov < 0f)
                    {
                        Flip();
                    }
  
                    body.velocity = new Vector2(horizontalMov * speed, body.velocity.y);
                    
                }
            }
            else
            {
                if (HitFromRight == true)
                {
                    body.velocity = new Vector2(-KnockBackSTR, KnockBackSTR / 1.5f);
                }
                if (HitFromRight == false)
                {
                    body.velocity = new Vector2(KnockBackSTR, KnockBackSTR / 1.5f);
                }
                KnockBackTimer -= Time.deltaTime;
            }

            if (hasWallJump == true)
            {
                WallSlide();
                WallJump();

                if (hasDoubleJump == true)
                {
                    if (OnWall())
                    {
                        UsedSecondJump = 0;
                        SecondJump = true;
                    }
                }
            }
        }

    }

    #region Movement & Jump
    public void Move(InputAction.CallbackContext context)
    {
        horizontalMov = context.ReadValue<Vector2>().x;
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (GameManager.Instance.GameIsPaused == false)
        {
            if (PowerSelected == null)
            {
                if (isDashing == false)
                {

                    if (hasDoubleJump == false)
                    {

                        if (context.performed)
                        {
                            JumpBufferCounter = JumpBufferTime;
                            IsJumping = true;
                        }
                        else
                        {
                            JumpBufferCounter -= Time.deltaTime;
                        }
                        if (JumpBufferCounter > 0 && canJump == true)
                        {
                            body.velocity = new Vector2(body.velocity.x, JumpStrength);
                            JumpBufferCounter = 0;
                            IsJumping = true;
                        }


                        if (context.canceled && body.velocity.y > 0f)
                        {
                            body.velocity = new Vector2(body.velocity.x, body.velocity.y * 0.5f);
                            IsJumping = true;
                        }

                        if (context.performed && WallJumpCounter > 0)
                        {
                            WallJumping = true;
                            body.velocity = new Vector2(WallJumpDirection * WallJumpStrength.x, WallJumpStrength.y);
                            WallJumpCounter = 0;
                            EnableTrailRenderer(1, Color.red, Color.clear);
                            if (transform.localScale.x != WallJumpDirection)
                            {
                                facingRight = !facingRight;
                                Vector3 localScale = transform.localScale;
                                localScale.x *= -1f;
                                transform.localScale = localScale;
                            }

                            Invoke(nameof(StopWallJump), WallJumpDuration);
                        }
                    }

                    if (hasDoubleJump == true)
                    {

                        if (context.performed)
                        {

                            JumpBufferCounter = JumpBufferTime;
                            IsJumping = true;

                            if (UsedSecondJump == 0)
                            {
                                SecondJump = true;
                            }

                        }
                        else
                        {
                            JumpBufferCounter -= Time.deltaTime;
                        }
                        if (JumpBufferCounter > 0 && canJump == true)
                        {

                            body.velocity = new Vector2(body.velocity.x, JumpStrength);
                            JumpBufferCounter = 0;
                            IsJumping = true;
                            if (UsedSecondJump == 0)
                            {
                                SecondJump = true;
                            }
                            UsedSecondJump = 0;
                            EnableTrailRenderer(1, Color.blue, Color.clear);
                        }
                        if (SecondJump == true)
                        {
                            body.velocity = new Vector2(body.velocity.x, JumpStrength);
                            JumpBufferCounter = 0;
                            IsJumping = true;
                            UsedSecondJump++;
                            SecondJump = false;
                            EnableTrailRenderer(1, Color.blue, Color.clear);
                        }

                        if (context.canceled && body.velocity.y > 0f)
                        {
                            body.velocity = new Vector2(body.velocity.x, body.velocity.y * 0.5f);
                            IsJumping = true;
                        }

                        if (context.performed && WallJumpCounter > 0)
                        {
                            WallJumping = true;
                            body.velocity = new Vector2(WallJumpDirection * WallJumpStrength.x, WallJumpStrength.y);
                            WallJumpCounter = 0;
                            EnableTrailRenderer(1, Color.red, Color.clear);
                            if (transform.localScale.x != WallJumpDirection)
                            {
                                facingRight = !facingRight;
                                Vector3 localScale = transform.localScale;
                                localScale.x *= -1f;
                                transform.localScale = localScale;
                            }

                            Invoke(nameof(StopWallJump), WallJumpDuration);
                        }


                    }
                }
            }

            if (LevelSelected != null && OnGround())
            {
                LevelSelected.GetComponent<LevelDoors>().StartLevel();
            }

            if (PowerSelected != null && OnGround())
            {
                PowerSelected.GetComponent<AbilityStore>().AddOrRemovePower();
            }
        }
    }

    public IEnumerator CoyoteTimeCountdown()
    {
        yield return new WaitForSeconds(CoyoteTime);
        canJump = false;
    }

    public IEnumerator EnableTrailRenderer(float AmountOfTime, Color startColor, Color endColor)
    {
        Trail.emitting = true;
        Trail.startColor = startColor;
        Trail.endColor = endColor;
        yield return new WaitForSeconds(AmountOfTime);
        Trail.emitting = false;
    }
    public void Flip()
    {
        if (isDashing == false)
        {
            facingRight = !facingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }
    #endregion

    #region Abilities

    public void Dash(InputAction.CallbackContext context)
    {
        if (context.performed && hasDash == true && canDash == true)
        {
            StartCoroutine(ExecuteDash());
            StartCoroutine(EnableTrailRenderer(DashLength, Color.red, Color.clear));
        }
    }

    private IEnumerator ExecuteDash()
    {
        canDash = false;
        isDashing = true;
        float Grav = body.gravityScale;
        body.gravityScale = 0;
        body.velocity = new Vector2(transform.localScale.x * DashStrength, 0f);
        yield return new WaitForSeconds(DashLength);
        body.gravityScale = Grav;
        isDashing = false;
        yield return new WaitForSeconds(DashCooldown);
        canDash = true;
    }

    public void BladeSlash(InputAction.CallbackContext context)
    {

    }

    public void WallSlide()
    {
        if (OnWall() && !OnGround() && horizontalMov != 0)
        {
            Sliding = true;
            body.velocity = new Vector2(body.velocity.x, Mathf.Clamp(body.velocity.y, -SlidingSpeed, float.MaxValue));
        }
        else
        {
            Sliding = false;
        }
    }

    public void WallJump()
    {
        if (Sliding)
        {
            WallJumping = false;
            WallJumpDirection = -transform.localScale.x;
            WallJumpCounter = WallJumpTime;
            CancelInvoke(nameof(StopWallJump));
        }
        else
        {
            WallJumpCounter -= Time.deltaTime;
        }
    }

    public void StopWallJump()
    {
        WallJumping = false;
    }

    public void Rewind(InputAction.CallbackContext context)
    {
        if (context.performed && HasRewind && !isRewinding && !RewindOnCooldown)
        {
            StartCoroutine(Rewind());
            StartCoroutine(EnableTrailRenderer(1, Color.blue, Color.clear));
        }
    }

    IEnumerator Rewind()
    {
        isRewinding = true;
        float segmentTime = rewindTime / rewindPositions.Count;
        if (Timer.Instance != null)
        {
            Timer.Instance.TimerActive = false;
        }

        for (int i = rewindPositions.Count - 1; i >= 0; i--)
        {
            Vector3 startPosition = transform.position;
            Vector3 targetPosition = rewindPositions[i];
            float elapsedTime = 0;

            while (elapsedTime < segmentTime)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, elapsedTime / segmentTime);
                elapsedTime += Time.deltaTime;
                if (Timer.Instance != null)
                {
                    Timer.Instance.RemainingTime += Time.deltaTime;
                    Timer.Instance.TimeSpent -= Time.deltaTime;
                }
                yield return null;
            }

            transform.position = targetPosition;
        }

        isRewinding = false;
        RewindOnCooldown = true;
        if (Timer.Instance != null)
        {
            Timer.Instance.TimerActive = true;
        }
        yield return new WaitForSeconds(rewindCooldown);
        RewindOnCooldown = false;
    }

    public void StartSaving()
    {
        StartCoroutine(SavePosition());
    }

    IEnumerator SavePosition()
    {
        while (true)
        {
            if (!isRewinding)
            {
                if (rewindPositions.Count == 3)
                {
                    rewindPositions.RemoveAt(0);
                }
                rewindPositions.Add(transform.position);
            }
            yield return new WaitForSeconds(rewindInterval);
        }
    }

    #endregion

    #region Movement Constraints

    private bool OnGround()
    {
        float extraDistance = 0.1f;
        RaycastHit2D rayHit = Physics2D.BoxCast(Collider.bounds.center, Collider.bounds.size, 0f, Vector2.down, extraDistance, GroundLayer);
        
        return rayHit.collider != null;
    }

    private bool OnWall()
    {
        return Physics2D.OverlapCircle(WallSensor.position, 0.2f, WallLayer);
    }

    #endregion

    public void TakeDamage(Transform DamageSource, float DamageNumber)
    {
        KnockBackTimer = KnockBackTime;
        if (transform.position.x <= DamageSource.transform.position.x)
        {
            HitFromRight = true;
        }
        if (transform.position.x >= DamageSource.transform.position.x)
        {
            HitFromRight = false;
        }

        GameObject DisplayedDamage = Instantiate(DamageTaken, transform.position, Quaternion.identity) as GameObject;

        Timer.Instance.RemainingTime -= DamageNumber;
        Timer.Instance.TimeSpent += DamageNumber;

        DisplayedDamage.transform.GetChild(0).GetComponent<TextMesh>().text = "-" + DamageNumber.ToString() + "s";
    }

    public void Pause(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (GameManager.Instance.GameIsPaused == false)
            {
                GameManager.Instance.PauseGame();
                Cursor.visible = true;
            }
            else if (GameManager.Instance.GameIsPaused == true)
            {
                GameManager.Instance.UnPauseGame();
                Cursor.visible = false;
            }
        }
    }


}
