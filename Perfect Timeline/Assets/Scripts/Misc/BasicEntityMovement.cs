using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEntityMovement : MonoBehaviour
{
    [SerializeField] Transform[] Objectives;
    public float MovementSpeed;
    [SerializeField] int CurrentObjective;

    private void Start()
    {
        CurrentObjective = Random.Range(0, Objectives.Length);
    }

    public virtual void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, Objectives[CurrentObjective].position, MovementSpeed * Time.deltaTime);

        if (Vector2.Distance(transform.position, Objectives[CurrentObjective].position) <= 0.2f)
        {
            CurrentObjective++;
        }
        if (CurrentObjective >= Objectives.Length)
        {
            CurrentObjective = 0;
        }
    }

}
