using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public TMPro.TextMeshProUGUI TimeLeft;
    public void GoToLevelSelect()
    {
        Cursor.visible = false;
        SceneManager.LoadScene("PlaceholderROOT");
        Cursor.visible = false;
        GameManager.Instance.UnPauseGame();
    }

    public void GoToMenu()
    {
        Cursor.visible = true;
        SceneManager.LoadScene("MenuScene");
        GameManager.Instance.UnPauseGame();
    }

    public void Options()
    {
        Debug.Log("Went to options");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
