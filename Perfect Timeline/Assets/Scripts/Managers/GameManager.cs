using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  public static GameManager Instance;

    #region Time Stuff
    [Header("Time Controls")]
    [Tooltip ("The total amount of time (in seconds) the player has to finish the game")]
    public float TotalTime;
    [Tooltip("An array that keeps track of the time left after completing each level.")]
    public List<float> TimeAfterLevels;
    [Tooltip("The amount of time spent on each level")]
    public List<float> TimeSpent;
    [Tooltip("The time left before Game Over. Calculated by substracting all the time spent on completed levels from TotalTime")]
    public float RemainingTime;
    [Tooltip("The amount of times the player has beaten their previous best time for each level")]
    public List<int> TimesBeaten; // 0 means the player hasn't beaten their original time, each number after that represents the amount of times the player has beaten the previous time.
    #endregion
    #region Ability Stuff
    [Header("Control Abilities")]
    public bool HasDoubleJump = false;
    public float DoubleJumpCost;
    public bool HasDash = false;
    public float DashCost;
    public bool HasWallJump = false;
    public float WallJumpCost;
    public bool HasRewind = false;
    public float RewindCost;
    public List<bool> UsedDoubleJump;
    public List<bool> UsedDash;
    public List<bool> UsedWallJump;
    public List<bool> UsedRewind;
    #endregion
    #region PauseMenu
    public bool GameIsPaused = false;
    [SerializeField] GameObject PauseScreen;
    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);   
    }
    private void Start()
    {
        RecalculateTimes();
        CalculateTimeLeft();
        Cursor.visible = false;
    }

    private void Update()
    {
        if (LevelManager.Instance == null)
        {
            RecalculateTimes();
            CalculateTimeLeft();
        }
    }
    public void CalculateTimeLeft()
    {
        float Time = TotalTime;

        for (int i = 0; i < TimeSpent.Count; i++)
        {
            Time = Time - TimeSpent[i];
            TimeAfterLevels[i] = Time;
        }

        RemainingTime = Time;

        float minutes = Mathf.FloorToInt(RemainingTime / 60);
        float seconds = Mathf.FloorToInt(RemainingTime % 60);
    }

    public void RecalculateTimes()
    {

        if (TimeSpent.Count > 0)
        {
            for (int i = 0; i <= TimeSpent.Count; i++)
            {
                if (i == 0)
                {
                    TimeAfterLevels[i] = TotalTime - TimeSpent[i];
                }
                else if(TimeSpent.Count > 1 && i != 0 && i != 1)
                {
                    TimeAfterLevels[i] = TimeAfterLevels[i - 1] - TimeSpent[i];
                }
            }
        }        
    }
     
    public void PauseGame()
    {
        GameIsPaused = true;
        PauseScreen.SetActive(true);
        Cursor.visible = false;
        if (LevelManager.Instance != null)
        {
            
            Timer.Instance.TimerActive = false;

            float TimeToShow = Timer.Instance.RemainingTime;
            float minutes = Mathf.FloorToInt(TimeToShow / 60);
            float seconds = Mathf.FloorToInt(TimeToShow % 60);

            PauseScreen.GetComponent<PauseMenu>().TimeLeft.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }else
        {
            float TimeToShow = RemainingTime;
            float minutes = Mathf.FloorToInt(TimeToShow / 60);
            float seconds = Mathf.FloorToInt(TimeToShow % 60);

            PauseScreen.GetComponent<PauseMenu>().TimeLeft.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        
    }

    public void UnPauseGame()
    {
        GameIsPaused = false;
        PauseScreen.SetActive(false);
        Cursor.visible = true;
        if (Timer.Instance != null)
        {
            Timer.Instance.TimerActive = true;
        }
    }
}
